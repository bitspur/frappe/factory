variable "REGISTRY" {
  default = "docker.io/library"
}
variable "BENCH_DIR" {
  default = "/home/frappe/frappe-bench"
}
variable "DEBIAN_VERSION" {}
variable "ERPNEXT_VERSION" {}
variable "FRAPPE_BRANCH" {}
variable "FRAPPE_PATH" {}
variable "NODE_VERSION" {}
variable "PYTHON_VERSION" {}

group "default" {
  targets = [
    "base",
    "builder",
    "business",
    "core",
    "devcontainer",
    "ecommerce",
    "education",
    "erpnext",
    "frappe",
  ]
}

target "base" {
  context    = "base"
  dockerfile = "Dockerfile"
  platforms = [
    "linux/amd64",
    # "linux/arm64"
  ]
  args = {
    BENCH_DIR      = "${BENCH_DIR}"
    DEBIAN_VERSION = "${DEBIAN_VERSION}"
    NODE_VERSION   = "${NODE_VERSION}"
    PYTHON_VERSION = "${PYTHON_VERSION}"
  }
  tags = [
    "${REGISTRY}/base:${DEBIAN_VERSION}",
    "${REGISTRY}/base:${DEBIAN_VERSION}-${PYTHON_VERSION}",
    "${REGISTRY}/base:${DEBIAN_VERSION}-${PYTHON_VERSION}-${NODE_VERSION}",
    "${REGISTRY}/base:latest",
  ]
}

target "builder" {
  context    = "builder"
  dockerfile = "Dockerfile"
  platforms = [
    "linux/amd64",
    # "linux/arm64"
  ]
  args = {
    BENCH_DIR      = "${BENCH_DIR}"
    DEBIAN_VERSION = "${DEBIAN_VERSION}"
    NODE_VERSION   = "${NODE_VERSION}"
    PYTHON_VERSION = "${PYTHON_VERSION}"
  }
  tags = [
    "${REGISTRY}/builder:${DEBIAN_VERSION}",
    "${REGISTRY}/builder:${DEBIAN_VERSION}-${PYTHON_VERSION}",
    "${REGISTRY}/builder:${DEBIAN_VERSION}-${PYTHON_VERSION}-${NODE_VERSION}",
    "${REGISTRY}/builder:latest",
  ]
  contexts = {
    base = "target:base"
  }
}

target "frappe" {
  context    = "frappe"
  dockerfile = "Dockerfile"
  platforms = [
    "linux/amd64",
    # "linux/arm64"
  ]
  args = {
    BENCH_DIR     = "${BENCH_DIR}"
    FRAPPE_BRANCH = "${FRAPPE_BRANCH}"
    FRAPPE_PATH   = "${FRAPPE_PATH}"
  }
  tags = [
    "${REGISTRY}/frappe:${FRAPPE_BRANCH}",
    "${REGISTRY}/frappe:${FRAPPE_BRANCH}-${DEBIAN_VERSION}",
    "${REGISTRY}/frappe:${FRAPPE_BRANCH}-${DEBIAN_VERSION}-${PYTHON_VERSION}",
    "${REGISTRY}/frappe:${FRAPPE_BRANCH}-${DEBIAN_VERSION}-${PYTHON_VERSION}-${NODE_VERSION}",
    "${REGISTRY}/frappe:latest",
  ]
  contexts = {
    base    = "target:base"
    builder = "target:builder"
  }
}

target "core" {
  context    = "core"
  dockerfile = "Dockerfile"
  platforms = [
    "linux/amd64",
    # "linux/arm64"
  ]
  args = {
    BENCH_DIR = "${BENCH_DIR}"
  }
  tags = [
    "${REGISTRY}/core:${FRAPPE_BRANCH}",
    "${REGISTRY}/core:${FRAPPE_BRANCH}-${DEBIAN_VERSION}",
    "${REGISTRY}/core:${FRAPPE_BRANCH}-${DEBIAN_VERSION}-${PYTHON_VERSION}",
    "${REGISTRY}/core:${FRAPPE_BRANCH}-${DEBIAN_VERSION}-${PYTHON_VERSION}-${NODE_VERSION}",
    "${REGISTRY}/core:latest",
  ]
  contexts = {
    base    = "target:base"
    builder = "target:builder"
    frappe  = "target:frappe"
  }
}

target "erpnext" {
  context    = "erpnext"
  dockerfile = "Dockerfile"
  platforms = [
    "linux/amd64",
    # "linux/arm64"
  ]
  args = {
    BENCH_DIR       = "${BENCH_DIR}"
    ERPNEXT_VERSION = "${ERPNEXT_VERSION}"
  }
  tags = [
    "${REGISTRY}/erpnext:${ERPNEXT_VERSION}",
    "${REGISTRY}/erpnext:${ERPNEXT_VERSION}-${FRAPPE_BRANCH}",
    "${REGISTRY}/erpnext:${ERPNEXT_VERSION}-${FRAPPE_BRANCH}-${DEBIAN_VERSION}",
    "${REGISTRY}/erpnext:${ERPNEXT_VERSION}-${FRAPPE_BRANCH}-${DEBIAN_VERSION}-${PYTHON_VERSION}",
    "${REGISTRY}/erpnext:${ERPNEXT_VERSION}-${FRAPPE_BRANCH}-${DEBIAN_VERSION}-${PYTHON_VERSION}-${NODE_VERSION}",
    "${REGISTRY}/erpnext:latest",
  ]
  contexts = {
    base    = "target:base"
    builder = "target:builder"
    core    = "target:core"
  }
}

target "business" {
  context    = "business"
  dockerfile = "Dockerfile"
  platforms = [
    "linux/amd64",
    # "linux/arm64"
  ]
  args = {
    BENCH_DIR = "${BENCH_DIR}"
  }
  tags = [
    "${REGISTRY}/business:${ERPNEXT_VERSION}",
    "${REGISTRY}/business:${ERPNEXT_VERSION}-${FRAPPE_BRANCH}",
    "${REGISTRY}/business:${ERPNEXT_VERSION}-${FRAPPE_BRANCH}-${DEBIAN_VERSION}",
    "${REGISTRY}/business:${ERPNEXT_VERSION}-${FRAPPE_BRANCH}-${DEBIAN_VERSION}-${PYTHON_VERSION}",
    "${REGISTRY}/business:${ERPNEXT_VERSION}-${FRAPPE_BRANCH}-${DEBIAN_VERSION}-${PYTHON_VERSION}-${NODE_VERSION}",
    "${REGISTRY}/business:latest",
  ]
  contexts = {
    base    = "target:base"
    builder = "target:builder"
    erpnext = "target:erpnext"
  }
}

target "ecommerce" {
  context    = "ecommerce"
  dockerfile = "Dockerfile"
  platforms = [
    "linux/amd64",
    # "linux/arm64"
  ]
  args = {
    BENCH_DIR = "${BENCH_DIR}"
  }
  tags = [
    "${REGISTRY}/ecommerce:${ERPNEXT_VERSION}",
    "${REGISTRY}/ecommerce:${ERPNEXT_VERSION}-${FRAPPE_BRANCH}",
    "${REGISTRY}/ecommerce:${ERPNEXT_VERSION}-${FRAPPE_BRANCH}-${DEBIAN_VERSION}",
    "${REGISTRY}/ecommerce:${ERPNEXT_VERSION}-${FRAPPE_BRANCH}-${DEBIAN_VERSION}-${PYTHON_VERSION}",
    "${REGISTRY}/ecommerce:${ERPNEXT_VERSION}-${FRAPPE_BRANCH}-${DEBIAN_VERSION}-${PYTHON_VERSION}-${NODE_VERSION}",
    "${REGISTRY}/ecommerce:latest",
  ]
  contexts = {
    base     = "target:base"
    builder  = "target:builder"
    business = "target:business"
  }
}

target "education" {
  context    = "education"
  dockerfile = "Dockerfile"
  platforms = [
    "linux/amd64",
    # "linux/arm64"
  ]
  args = {
    BENCH_DIR = "${BENCH_DIR}"
  }
  tags = [
    "${REGISTRY}/education:${ERPNEXT_VERSION}",
    "${REGISTRY}/education:${ERPNEXT_VERSION}-${FRAPPE_BRANCH}",
    "${REGISTRY}/education:${ERPNEXT_VERSION}-${FRAPPE_BRANCH}-${DEBIAN_VERSION}",
    "${REGISTRY}/education:${ERPNEXT_VERSION}-${FRAPPE_BRANCH}-${DEBIAN_VERSION}-${PYTHON_VERSION}",
    "${REGISTRY}/education:${ERPNEXT_VERSION}-${FRAPPE_BRANCH}-${DEBIAN_VERSION}-${PYTHON_VERSION}-${NODE_VERSION}",
    "${REGISTRY}/education:latest",
  ]
  contexts = {
    base      = "target:base"
    builder   = "target:builder"
    ecommerce = "target:ecommerce"
  }
}

target "devcontainer" {
  context    = ".."
  dockerfile = "docker/devcontainer/Dockerfile"
  platforms = [
    "linux/amd64",
    # "linux/arm64"
  ]
  args = {
    BENCH_DIR = "${BENCH_DIR}"
  }
  tags = [
    "${REGISTRY}/devcontainer:${ERPNEXT_VERSION}",
    "${REGISTRY}/devcontainer:latest",
  ]
}
