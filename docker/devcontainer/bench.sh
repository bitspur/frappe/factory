#!/bin/sh

set -e
if [ "$WORK_DIR" = "" ]; then
    if [ -d /workspace/development/frappe-bench ]; then
        WORK_DIR="/workspace/development"
    elif [ -d /workspace/development/frappe/frappe-bench ]; then
        WORK_DIR="/workspace/development/frappe"
    fi
fi
if [ "$BENCH_DIR" = "" ]; then
    BENCH_DIR="$WORK_DIR/frappe-bench"
fi
if [ -d "$BENCH_DIR" ]; then
    cd "$BENCH_DIR"
fi
exec "$HOME/.local/bin/_bench" "$@"
