#!/bin/sh

set -e
. "$NVM_DIR/nvm.sh"
export WORK_DIR="${WORK_DIR:-/workspace/development}"
export DOTENV_FILE="${DOTENV_FILE:-$WORK_DIR/.env}"
export BENCH_DIR="${BENCH_DIR:-$WORK_DIR/frappe-bench}"
frappe-bootstrap
cd "$BENCH_DIR"
exec honcho start "$@"
