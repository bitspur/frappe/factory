#!/bin/sh

set -e
. "$NVM_DIR/nvm.sh"
WORK_DIR="${WORK_DIR:-/workspace/development}"
DOTENV_FILE="${DOTENV_FILE:-$WORK_DIR/.env}"
BENCH_DIR="${BENCH_DIR:-$WORK_DIR/frappe-bench}"
while [ $# -gt 0 ]; do
    case "$1" in
        --update)
            UPDATE=1
            ;;
        *)
            ;;
    esac
    shift
done
UPDATE="${UPDATE:-0}"
if [ ! -f "$DOTENV_FILE" ]; then
    DOTENV_DIR="$(dirname "$DOTENV_FILE")"
    for DOTENV_DEFAULT_FILE in "$DOTENV_DIR/.env.local" "$DOTENV_DIR/.env.example" "$DOTENV_DIR/.env.default"; do
        if [ -f "$DOTENV_DEFAULT_FILE" ]; then
            cp "$DOTENV_DEFAULT_FILE" "$DOTENV_FILE"
            break
        fi
    done
fi
. "$DOTENV_FILE"
cd "$WORK_DIR"
FRAPPE_ADMIN_PASSWORD="${FRAPPE_ADMIN_PASSWORD:-P@ssw0rd}"
FRAPPE_BASE_URL="${FRAPPE_BASE_URL:-http://localhost:8000}"
FRAPPE_BRANCH="${FRAPPE_BRANCH:-version-15-hotfix}"
FRAPPE_DB_HOST="${FRAPPE_DB_HOST:-$([ "$FRAPPE_DB_TYPE" = "postgres" ] && echo "$POSTGRES_HOSTNAME" || echo "$MYSQL_HOSTNAME")}"
FRAPPE_DB_NAME="${FRAPPE_DB_NAME:-frappe}"
FRAPPE_DB_PASSWORD="${FRAPPE_DB_PASSWORD:-$([ "$FRAPPE_DB_TYPE" = "postgres" ] && echo "$POSTGRES_PASSWORD" || echo "$MYSQL_PASSWORD")}"
FRAPPE_DB_PORT="${FRAPPE_DB_PORT:-$([ "$FRAPPE_DB_TYPE" = "postgres" ] && echo "$POSTGRES_PORT" || echo "$MYSQL_PORT")}"
FRAPPE_DB_TYPE="${FRAPPE_DB_TYPE:-postgres}"
FRAPPE_DB_USER="${FRAPPE_DB_USER:-$([ "$FRAPPE_DB_TYPE" = "postgres" ] && echo "$POSTGRES_USERNAME" || echo "$MYSQL_USERNAME")}"
FRAPPE_REPO="${FRAPPE_REPO:-https://github.com/frappe/frappe.git}"
FRAPPE_STATE_DIR="$HOME/.local/state/frappe"
REDIS_CACHE_URI="${REDIS_CACHE_URI:-redis://localhost:6379}"
REDIS_QUEUE_URI="${REDIS_QUEUE_URI:-redis://localhost:6380}"
SITE_NAME="${SITE_NAME:-$(echo "$FRAPPE_BASE_URL" | sed 's|^https\?://||' | sed 's|:[0-9]\+$||')}"
_install_deps() {
    _APP_PATH="$1"
    _REAL_NAME="$3"
    if [ "$UPDATE" = "1" ] || \
        (! ls "$BENCH_DIR/env/lib/python$(echo "$PYTHON_VERSION" | \
            sed 's|\.[^.]*$||')/site-packages" | \
            grep -q "^$_REAL_NAME-[0-9]\+\(\.[0-9]\+\)*\(\-\?[a-zA-Z]\+[0-9]*\)*\.dist-info$"); then
        uv pip install -e "$_APP_PATH" --link-mode=copy
        if [ -f "$_APP_PATH/dev-requirements.txt" ]; then
            uv pip install -r "$_APP_PATH/dev-requirements.txt" --link-mode=copy
        elif [ -f "$_APP_PATH/pyproject.toml" ]; then
            local _DEPS="$(python3 -c '
try:
    from tomli import loads
except ImportError:
    from tomllib import loads
import sys
with open(sys.argv[1]) as f:
    pyproject_config = loads(f.read())
deps = ""
try:
    for pkg, version in pyproject_config["tool"]["bench"]["dev-dependencies"].items():
        op = "==" if "=" not in version else ""
        deps += f"{pkg}{op}{version} "
    print(deps.strip())
except KeyError:
    print("")
' "$_APP_PATH/pyproject.toml")"
            [ "$_DEPS" != "" ] && uv pip install $_DEPS --link-mode=copy
        fi
    fi
    if [ -f "$_APP_PATH/package.json" ]; then
        (cd "$_APP_PATH" && yarn)
    fi
}
if [ "$FRAPPE_DB_TYPE" = "postgres" ]; then
    if ! PGPASSWORD="$FRAPPE_DB_PASSWORD" psql -h "$FRAPPE_DB_HOST" -p "$FRAPPE_DB_PORT" -U "$FRAPPE_DB_USER" -d "$FRAPPE_DB_NAME" -c "SELECT 1" >/dev/null 2>&1; then
        rm -rf "$BENCH_DIR" 2>/dev/null || true
    fi
else
    if ! mysql -h "$FRAPPE_DB_HOST" -P "$FRAPPE_DB_PORT" -u "$FRAPPE_DB_USER" -p"$FRAPPE_DB_PASSWORD" "$FRAPPE_DB_NAME" -e "SELECT 1" >/dev/null 2>&1; then
        rm -rf "$BENCH_DIR" 2>/dev/null || true
    fi
fi
PYTHON_VERSION="$(python --version | cut -d' ' -f2)"
mkdir -p "$FRAPPE_STATE_DIR/benches"
FRAPPE_STATE_BENCH_DIR="$FRAPPE_STATE_DIR/benches/$(echo "$BENCH_DIR:${PYTHON_VERSION}_$(uname -m)" | sha256sum | cut -d' ' -f1)"
if [ ! -f "$BENCH_DIR/apps/frappe/package.json" ] || [ ! -f "$BENCH_DIR/Procfile" ]; then
    rm -rf "$BENCH_DIR" 2>/dev/null || true
    if [ -f "$FRAPPE_STATE_DIR/apps/frappe/package.json" ] && [ -f "$FRAPPE_STATE_BENCH_DIR/Procfile" ]; then
        if [ -f "$FRAPPE_STATE_BENCH_DIR/Procfile" ]; then
            cp -r "$FRAPPE_STATE_BENCH_DIR" "$BENCH_DIR"
            if [ -L "$BENCH_DIR/env/bin/python3" ] && [ ! -e "$BENCH_DIR/env/bin/python" ]; then
                echo "orphaned python virtualenv" >&2
                exit 1
            fi
        fi
        (cd "$FRAPPE_STATE_DIR/apps/frappe" && git add . && git reset --hard && \
            (git rev-parse --is-shallow-repository 2>/dev/null | grep -q true && git pull --unshallow || git pull))
        rm -rf "$BENCH_DIR/apps" 2>/dev/null || true
        rm -rf "$BENCH_DIR/sites/$SITE_NAME" 2>/dev/null || true
        mkdir -p "$BENCH_DIR/apps"
        mkdir -p "$BENCH_DIR/sites"
        cp -r "$FRAPPE_STATE_DIR/apps/frappe" "$BENCH_DIR/apps"
        (cd "$BENCH_DIR" && bench build --app frappe)
    else
        bench init \
            --skip-redis-config-generation \
            --frappe-path="$FRAPPE_REPO" \
            --frappe-branch="$FRAPPE_BRANCH" \
            frappe-bench
        mkdir -p "$FRAPPE_STATE_DIR/apps"
        rm -rf "$FRAPPE_STATE_BENCH_DIR" 2>/dev/null || true
        cp -r "$BENCH_DIR" "$FRAPPE_STATE_BENCH_DIR"
        rm -rf "$FRAPPE_STATE_BENCH_DIR/apps" 2>/dev/null || true
        mkdir -p "$FRAPPE_STATE_BENCH_DIR/apps"
        (cd "$BENCH_DIR/apps/frappe" && git add . && git reset --hard && \
            (git rev-parse --is-shallow-repository 2>/dev/null | grep -q true && git pull --unshallow || git pull))
        rm -rf "$FRAPPE_STATE_DIR/apps/frappe" 2>/dev/null || true
        cp -r "$BENCH_DIR/apps/frappe" "$FRAPPE_STATE_DIR/apps"
    fi
fi
echo frappe > "$BENCH_DIR/sites/apps.txt"
cd "$BENCH_DIR"
. env/bin/activate
cat "$BENCH_DIR/sites/common_site_config.json" | \
    jq 'del(.redis_cache, .redis_queue, .redis_socketio)' > _common_site_config.json && \
    mv _common_site_config.json "$BENCH_DIR/sites/common_site_config.json"
bench set-config -g db_type "$FRAPPE_DB_TYPE"
bench set-config -g developer_mode 1
bench set-config -g redis_cache "$REDIS_CACHE_URI"
bench set-config -g redis_queue "$REDIS_QUEUE_URI"
bench set-config -g redis_socketio "$REDIS_QUEUE_URI"
if [ ! -f "$BENCH_DIR/sites/$SITE_NAME/site_config.json" ]; then
    if [ "$FRAPPE_DB_TYPE" = "postgres" ]; then
        PGPASSWORD="$FRAPPE_DB_PASSWORD" psql -h "$FRAPPE_DB_HOST" -p "$FRAPPE_DB_PORT" -U "$FRAPPE_DB_USER" -c \
            "SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = '$FRAPPE_DB_NAME';"
    else
        mysql -h "$FRAPPE_DB_HOST" -P "$FRAPPE_DB_PORT" -u "$FRAPPE_DB_USER" -p"$FRAPPE_DB_PASSWORD" -e \
            "SELECT GROUP_CONCAT(CONCAT('KILL ',id,';') SEPARATOR ' ') FROM INFORMATION_SCHEMA.PROCESSLIST WHERE db='$FRAPPE_DB_NAME' INTO @kill_list; SET @kill_list = IFNULL(@kill_list,'SELECT 1'); PREPARE kill_stmt FROM @kill_list; EXECUTE kill_stmt; DEALLOCATE PREPARE kill_stmt;"
    fi
    if [ "$FRAPPE_DB_HOST" = "localhost" ]; then
        if [ "$FRAPPE_DB_TYPE" = "postgres" ]; then
            PGPASSWORD="$FRAPPE_DB_PASSWORD" psql -h "$FRAPPE_DB_HOST" -p "$FRAPPE_DB_PORT" -U "$FRAPPE_DB_USER" -c \
                "DROP DATABASE IF EXISTS $FRAPPE_DB_NAME;"
        else
            mysql -h "$FRAPPE_DB_HOST" -P "$FRAPPE_DB_PORT" -u "$FRAPPE_DB_USER" -p"$FRAPPE_DB_PASSWORD" -e \
                "DROP DATABASE IF EXISTS $FRAPPE_DB_NAME;"
        fi
    fi
    bench new-site \
        --admin-password="$FRAPPE_ADMIN_PASSWORD" \
        --db-host="$FRAPPE_DB_HOST" \
        --db-name="$FRAPPE_DB_NAME" \
        --db-password="$FRAPPE_DB_PASSWORD" \
        --db-port="$FRAPPE_DB_PORT" \
        --db-root-password="$FRAPPE_DB_PASSWORD" \
        --db-type="$FRAPPE_DB_TYPE" \
        "$SITE_NAME"
fi
sed -i "s|\.nvm/versions/node/v\([0-9]\+\.\?\)\+/bin/node|\.nvm/versions/node/v$NODE_VERSION/bin/node|g" "$BENCH_DIR/Procfile"
bench use "$SITE_NAME"
cat ../apps.jsonc | grep -v "^[[:space:]]*\/\/" | jq -r '.[] | "BRANCH=\(.branch) URL=\(.url) NAME=\(.name)"' | while IFS= read -r line; do
    eval "$line"
    if [ "$URL" = "" ] || [ "$URL" = "null" ]; then
        continue
    fi
    if [ "$BRANCH" = "" ] || [ "$BRANCH" = "null" ]; then
        BRANCH="main"
    fi
    if [ "$NAME" = "" ] || [ "$NAME" = "null" ]; then
        NAME="$(basename "$URL" .git)"
        if [ "$NAME" = "" ] || [ "$NAME" = "frappe" ] || [ "$NAME" = "app" ]; then
            continue
        fi
    fi
    _ID="$(echo "$NAME:$URL" | sha256sum | cut -d' ' -f1)"
    if [ -d "$FRAPPE_STATE_DIR/apps/$_ID/.git" ]; then
        {
            cd "$FRAPPE_STATE_DIR/apps/$_ID"
            git add .
            git reset --hard
            if [ "$UPDATE" = "1" ]; then
                git fetch --all
            fi
            git checkout "$BRANCH"
            git merge "origin/$BRANCH"
            cd -
        }
    else
        git clone --branch "$BRANCH" "$URL" "$FRAPPE_STATE_DIR/apps/$_ID"
    fi
    REAL_NAME="$( (cat "$FRAPPE_STATE_DIR/apps/$_ID/pyproject.toml" 2>/dev/null || true; cat "$FRAPPE_STATE_DIR/apps/$_ID/setup.py" 2>/dev/null || true) | \
        grep -E '^\s*name\s*=\s*' | head -n1 | sed "s|^\s*name\s*=\s*[\"']\([^\"']*\)[\"']\s*,\?|\1|g")"
    if [ "$REAL_NAME" = "" ]; then
        REAL_NAME="$NAME"
    fi
    echo >> "$BENCH_DIR/sites/apps.txt"
    echo "$REAL_NAME" >> "$BENCH_DIR/sites/apps.txt"
    sed -i '/^$/d' "$BENCH_DIR/sites/apps.txt"
    if [ "$UPDATE" = "1" ] || [ ! -d "$BENCH_DIR/apps/$REAL_NAME/$REAL_NAME/__pycache__" ]; then
        rm -rf "$BENCH_DIR/apps/$REAL_NAME" 2>/dev/null || true
        cp -r "$FRAPPE_STATE_DIR/apps/$_ID" "$BENCH_DIR/apps/$REAL_NAME"
        _install_deps "$BENCH_DIR/apps/$REAL_NAME" "$REAL_NAME"
        bench build --app "$REAL_NAME"
        bench install-app "$REAL_NAME"
    fi
done
if [ -f "/workspace/app/pyproject.toml" ] || [ -f "/workspace/app/setup.py" ]; then
    APP_DIR="/workspace/app"
fi
if [ "$APP_DIR" != "" ] && [ -f "$APP_DIR/pyproject.toml" ] || [ -f "$APP_DIR/setup.py" ]; then
    REAL_NAME="$( (cat "$APP_DIR/pyproject.toml" 2>/dev/null || true; cat "$APP_DIR/setup.py" 2>/dev/null || true) | \
        grep -E '^\s*name\s*=\s*' | head -n1 | sed "s|^\s*name\s*=\s*[\"']\([^\"']*\)[\"']\s*,\?|\1|g")"
    if [ "$REAL_NAME" = "" ] && [ "$REAL_NAME" != "frappe" ]; then
        REAL_NAME="app"
    fi
    echo >> "$BENCH_DIR/sites/apps.txt"
    echo "$REAL_NAME" >> "$BENCH_DIR/sites/apps.txt"
    sed -i '/^$/d' "$BENCH_DIR/sites/apps.txt"
    rm -rf "$BENCH_DIR/apps/$REAL_NAME" 2>/dev/null || true
    ln -s "$APP_DIR" "$BENCH_DIR/apps/$REAL_NAME"
    if [ "$UPDATE" = "1" ] || \
        (! ls "$BENCH_DIR/env/lib/python$(echo "$PYTHON_VERSION" | \
            sed 's|\.[^.]*$||')/site-packages" | \
            grep -q "^$REAL_NAME-[0-9]\+\(\.[0-9]\+\)*\(\-\?[a-zA-Z]\+[0-9]*\)*\.dist-info$"); then
        _install_deps "$BENCH_DIR/apps/$REAL_NAME" "$REAL_NAME"
    fi
    if [ -f "$BENCH_DIR/apps/$REAL_NAME/package.json" ]; then
        (cd "$BENCH_DIR/apps/$REAL_NAME" && yarn)
    fi
    bench build --app "$REAL_NAME"
    bench install-app "$REAL_NAME"
fi
if [ "$(cat "$BENCH_DIR/sites/$SITE_NAME/site_config.json" | jq -r '.db_user // ""')" = "" ]; then
    bench --site "$SITE_NAME" set-config developer_mode 1
    bench --site "$SITE_NAME" set-config allow_tests 1
    bench --site "$SITE_NAME" set-config db_host "$FRAPPE_DB_HOST"
    bench --site "$SITE_NAME" set-config db_name "$FRAPPE_DB_NAME"
    bench --site "$SITE_NAME" set-config db_password "$FRAPPE_DB_PASSWORD"
    bench --site "$SITE_NAME" set-config db_port "$FRAPPE_DB_PORT"
    bench --site "$SITE_NAME" set-config db_user "$FRAPPE_DB_USER"
    bench --site "$SITE_NAME" clear-cache
fi
bench --site "$SITE_NAME" migrate
