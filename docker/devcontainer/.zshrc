export ZSH=$HOME/.oh-my-zsh
ZSH_THEME="spaceship"
plugins=(
    git
    zsh-autosuggestions
    zsh-completions
    zsh-syntax-highlighting
)
source $ZSH/oh-my-zsh.sh
DISABLE_AUTO_UPDATE=true
DISABLE_UPDATE_PROMPT=true
. "$NVM_DIR/nvm.sh"
. "$NVM_DIR/bash_completion"
alias m=mkpm
