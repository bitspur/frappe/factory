# File: /docker/core/Dockerfile
# Project: factory
# File Created: 26-01-2025 14:27:28
# Author: Clay Risser
# -----
# BitSpur (c) Copyright 2021 - 2025
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

FROM builder AS b
ARG BENCH_DIR=/home/frappe/frappe-bench
USER frappe
COPY --from=frappe --chown=frappe:frappe ${BENCH_DIR} ${BENCH_DIR}
COPY apps.jsonc /home/frappe/apps.jsonc
RUN set -x && \
	echo '{"socketio_port": 9000}' > ${BENCH_DIR}/sites/common_site_config.json && \
	cd ${BENCH_DIR} && \
	cat /home/frappe/apps.jsonc | grep -v "^[[:space:]]*\/\/" | jq -r '.[] | "BRANCH=\(.branch) URL=\(.url) NAME=\(.name)"' | while IFS= read -r _LINE; do \
	eval "$_LINE" && \
	if [ "$URL" = "" ] || [ "$URL" = "null" ]; then \
	continue; \
	fi && \
	if [ "$BRANCH" = "" ] || [ "$BRANCH" = "null" ]; then \
	BRANCH="main"; \
	fi && \
	if [ "$NAME" = "" ] || [ "$NAME" = "null" ]; then \
	NAME="$(basename "$URL" .git)"; \
	fi && \
	if [ "$NAME" = "" ] || [ "$NAME" = "frappe" ] || [ "$NAME" = "app" ]; then \
	continue; \
	fi && \
	mkdir -p /tmp/apps && \
	git clone --branch "$BRANCH" "$URL" "/tmp/apps/$NAME" && \
	REAL_NAME="$( (cat "/tmp/apps/$NAME/pyproject.toml" 2>/dev/null || true; cat "/tmp/apps/$NAME/setup.py" 2>/dev/null || true) | \
	grep -E '^\s*name\s*=\s*' | head -n1 | sed "s|^\s*name\s*=\s*[\"']\([^\"']*\)[\"']\s*,\?|\1|g")" && \
	if [ "$REAL_NAME" = "" ]; then \
	REAL_NAME="$NAME"; \
	fi && \
	mv "/tmp/apps/$NAME" "$BENCH_DIR/apps/$REAL_NAME" && \
	echo >> "$BENCH_DIR/sites/apps.txt" && \
	echo "$REAL_NAME" >> "$BENCH_DIR/sites/apps.txt" && \
	sed -i '/^$/d' "$BENCH_DIR/sites/apps.txt" && \
	. ${BENCH_DIR}/env/bin/activate && \
	uv pip install -e "$BENCH_DIR/apps/$REAL_NAME" --link-mode=copy && \
	if [ -f "$BENCH_DIR/apps/$REAL_NAME/package.json" ]; then \
	cd "$BENCH_DIR/apps/$REAL_NAME" && yarn; \
	fi && \
	cd ${BENCH_DIR} && \
	bench build --app "$REAL_NAME"; \
	done && \
	echo '{}' > ${BENCH_DIR}/sites/common_site_config.json && \
	find apps -mindepth 1 -path "*/.git" | xargs rm -fr && \
	rm -rf /home/frappe/.ssh /tmp/apps

FROM base
USER frappe
COPY --from=b --chown=frappe:frappe /home/frappe/frappe-bench /home/frappe/frappe-bench
WORKDIR /home/frappe/frappe-bench
VOLUME [ \
	"/home/frappe/frappe-bench/sites", \
	"/home/frappe/frappe-bench/sites/assets", \
	"/home/frappe/frappe-bench/logs" \
	]
CMD [ \
	"/home/frappe/frappe-bench/env/bin/gunicorn", \
	"--chdir=/home/frappe/frappe-bench/sites", \
	"--bind=0.0.0.0:8000", \
	"--threads=4", \
	"--workers=2", \
	"--worker-class=gthread", \
	"--worker-tmp-dir=/dev/shm", \
	"--timeout=120", \
	"--preload", \
	"frappe.app:application" \
	]
