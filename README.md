# erpnext-images

> custom images for erpnext

## Development

```py
from os import chdir
import frappe

chdir('/workspace/development/frappe-bench/sites')
frappe.init(site='development.localhost', sites_path='/workspace/development/frappe-bench/sites')
frappe.connect()
frappe.local.lang = frappe.db.get_default('lang')
frappe.db.connect()
```

```sh
bench --site development.localhost set-config -g developer_mode 1
bench --site development.localhost clear-cache
```

### Print Format

#### HTML Jinja Templates

You cannot create a custom `.html` print format unless your apps module is
responsible for the DocType of the print format. If so, it will be found in
the `<APP>/<MODULE>/print_format/<DOC_TYPE>` folder.

If not, then you can either use the new print format builder, or else you can
create the html in the custom html field (make sure `Custom Format` is selected).
